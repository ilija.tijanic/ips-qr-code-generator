### About the project

ips-qr-code-generator is a simple Java library for generating [IPS QR
codes](https://web.archive.org/web/20221220172841/https://nbs.rs/en/ciljevi-i-funkcije/platni-sistem/nbs-operator/ips-nbs/index.html).
The minimum requirement is <b>JDK 11</b>.

[//]: # (### How to get started)

[//]: # ()
[//]: # (#### Maven)

[//]: # ()
[//]: # (Add the following dependency to your pom.xml file:)

[//]: # ()
[//]: # (```)

[//]: # (<dependency>)

[//]: # (    <groupId>com.ingsoftware</groupId>)

[//]: # (    <artifactId>ips-qr-code-generator</artifactId>)

[//]: # (    <version>1.0.0</version>)

[//]: # (</dependency>)

[//]: # (```)

[//]: # (#### JAR)

[//]: # ()
[//]: # (Download the [JAR file]&#40;JAR_FILE_URL&#41; and add it to your project's classpath or run it.)

### Examples of use

#### Use as library:

```java
IpsQrCode ipsQrCode = new IpsQrCodeBuilder()
        .withIdentificationCode("PR")
        .withRecipientAccountNumber("330-8800107231701-92")
        .withRecipientName("Petar Petrovic Bulevar 1, 11000 Beograd")
        .withAmount("RSD1230,00")
        .withPayerName("Marko Markovic Ulica 2, 18000 Nis")
        .withPaymentCode("123")
        .withPaymentPurpose("transakcija po nalogu gradjana")
        .withRecipientReference("N9JF3T5SC7")
        .build();

IpsQrCodeGenerator ipsQrCodeGenerator = new IpsQrCodeGenerator(new ZxingQrCodeImageGenerator());
BufferedImage image = ipsQrCodeGenerator.generateImage(ipsQrCode, size);
```

#### Use as CLI:

ips-qr-code-generator is written as a library, but can also be used as a CLI application:

```
java -jar ips-qr-code-generator-1.0.0.jar
--identification-code PR \
--payer-name "@ingsoftware" \
--recipient-name "GitLab Ltd," \
--recipient-account-number "330-8800107231701-92" \
--payment-code "221" \
--amount "RSD1," \
--payment-purpose "Example usage" \
```

### Example of generated IPS QR code:

![Example of generated IPS QR code](/example_qr_code.png)