/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import com.ingsoftware.oss.ipsqrcodegenerator.exception.InvalidTagValueException;
import com.ingsoftware.oss.ipsqrcodegenerator.exception.MultipleMutuallyExclusiveTagsException;
import com.ingsoftware.oss.ipsqrcodegenerator.generation.IpsQrCodeGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IpsQrCodeGeneratorTest {

    @Test
    void getQrCodeTextualContent_ShouldReturnSanitizedContent_WhenDataIsValidPR() throws InvalidTagValueException, MultipleMutuallyExclusiveTagsException {
        IpsQrCode ipsQrCode = new IpsQrCodeBuilder()
                .withIdentificationCode("PR")
                .withVersion("01")
                .withCharacterSet("1")
                .withRecipientAccountNumber("330-8800107231701-92")
                .withRecipientName("Ime Prezime Adresa 1 Grad")
                .withAmount("RSD1230,00")
                .withPayerName("Ime Prezime Adresa 1 Grad")
                .withPaymentCode("123")
                .withPaymentPurpose("transakcija po nalogu gradjana")
                .withRecipientReference("nekipodaci")
                .build();
        String actual = IpsQrCodeGenerator.generateTextualContent(ipsQrCode);
        assertEquals("K:PR|V:01|C:1|R:330880010723170192|N:Ime Prezime Adresa 1 Grad|I:RSD1230,00|P:Ime Prezime Adresa 1 Grad|SF:123|S:transakcija po nalogu gradjana|RL:nekipodaci", actual);
    }

    @Test
    void getQrCodeTextualContent_ShouldThrowException_WhenDataIsInvalid() {
        IpsQrCode ipsQrCode = new IpsQrCodeBuilder().build();
        assertThrows(InvalidTagValueException.class, () -> IpsQrCodeGenerator.generateTextualContent(ipsQrCode));
    }

    @Test
    void getQrCodeTextualContent_ShouldThrowException_WhenTooManyMutuallyExclusiveTags() {
        IpsQrCode ipsQrCode = new IpsQrCodeBuilder()
                .withIdentificationCode("PR")
                .withReferenceNumber("9754567812154820012")
                .withRecipientReference("123")
                .build();
        assertThrows(MultipleMutuallyExclusiveTagsException.class, () -> IpsQrCodeGenerator.generateTextualContent(ipsQrCode));
    }

}