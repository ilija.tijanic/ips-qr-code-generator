/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RValidatorTest {

    private static final Validator VALIDATOR = Validators.R;

    @Test
    void validate_ShouldReturnTrue_WhenValueHasCorrectControlNumber() {
        boolean actual = VALIDATOR.validate("330880010723170192");
        assertTrue(actual);
    }

    @Test
    void validate_ShouldReturnFalse_WhenValueHasIncorrectControlNumber() {
        boolean actual = VALIDATOR.validate("330880010723170195");
        assertFalse(actual);
    }

    @Test
    void validate_ShouldReturnFalse_WhenValueLengthIsNotValid() {
        boolean actual = VALIDATOR.validate("3308800107231701");
        assertFalse(actual);
    }

    @Test
    void validate_ShouldReturnFalse_WhenValueContainsNonDigits() {
        boolean actual = VALIDATOR.validate("33-8800107231701A!");
        assertFalse(actual);
    }

}
