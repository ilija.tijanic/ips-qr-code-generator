/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VValidatorTest {

    private static final Validator VALIDATOR = Validators.V;

    @Test
    void validate_ShouldReturnTrue_WhenValueIsAllowed() {
        boolean actual = VALIDATOR.validate("01");
        assertTrue(actual);
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "0", "AB", "!c", " ", "10"})
    void validate_ShouldReturnFalse_WhenValueIsNotAllowed(String value) {
        boolean actual = VALIDATOR.validate(value);
        assertFalse(actual);
    }

}
