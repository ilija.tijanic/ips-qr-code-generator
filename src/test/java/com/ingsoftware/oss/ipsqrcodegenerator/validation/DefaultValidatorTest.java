/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.validation;

import com.ingsoftware.oss.ipsqrcodegenerator.Constants;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultValidatorTest {

    @Test
    void validate_ShouldReturnTrue_WhenValueIsNonEmptyString() {
        boolean actual = Validators.validateLength("123", Constants.Limits.SF);
        assertTrue(actual);
    }

    @Test
    void validate_ShouldReturnFalse_WhenValueIsNullEmptyString() {
        boolean actual = Validators.validateLength(null, Constants.Limits.SF);
        assertFalse(actual);
    }

    @Test
    void validate_ShouldReturnFalse_WhenValueIsEmptyString() {
        boolean actual = Validators.validateLength("", Constants.Limits.SF);
        assertFalse(actual);
    }

}