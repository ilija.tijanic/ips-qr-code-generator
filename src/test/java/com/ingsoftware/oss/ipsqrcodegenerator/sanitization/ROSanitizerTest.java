/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.sanitization;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ROSanitizerTest {

    private static final Sanitizer SANITIZER = Sanitizers.RO;

    @ParameterizedTest
    @ValueSource(strings = {"9754567812F48K012", "9754567812f48k012"})
    void sanitize_ShouldReturnSanitized_WhenValueHasCorrectLength(String value) {
        String actual = SANITIZER.sanitize(value);
        assertEquals("9754567812154820012", actual);
    }

}