/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.generation;

/**
 * Levels of error correction for QR code generation.
 * The level determines the percentage of the total QR code
 * that is allowed to be dirty or damaged without being unable to read.
 *
 * @see <a href="https://www.qrcode.com/en/about/error_correction.html">QR code error correction</a>
 */
public enum CorrectionLevel {

    /**
     * Can be damaged up to 7%
     */
    L,
    /**
     * Can be damaged up to 15%
     */
    M,
    /**
     * Can be damaged up to 25%
     */
    Q,
    /**
     * Can be damaged up to 30%
     */
    H

}
