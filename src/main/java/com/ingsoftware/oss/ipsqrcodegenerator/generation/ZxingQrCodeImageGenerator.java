/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.generation;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ingsoftware.oss.ipsqrcodegenerator.exception.QrCodeGenerationException;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * Generates a <code>BufferedImage</code> object representing a QR code using the ZXing ("zebra crossing") library.
 *
 * @see QrCodeImageGenerator
 */
public class ZxingQrCodeImageGenerator implements QrCodeImageGenerator {

    /**
     * @see QrCodeImageGenerator#generateBufferedImage(String, int, CorrectionLevel)
     */
    @Override
    public BufferedImage generateBufferedImage(String qrCodeContent, int size, CorrectionLevel correctionLevel)
            throws QrCodeGenerationException {
        try {
            QRCodeWriter writer = new QRCodeWriter();

            ErrorCorrectionLevel errorCorrectionLevel = ErrorCorrectionLevel.valueOf(correctionLevel.name());
            Map<EncodeHintType, Object> hints = new HashMap<>();
            hints.put(EncodeHintType.ERROR_CORRECTION, errorCorrectionLevel);

            BitMatrix bitMatrix = writer.encode(qrCodeContent, BarcodeFormat.QR_CODE, size, size, hints);

            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (Exception e) {
            throw new QrCodeGenerationException(e.getMessage());
        }
    }

}
