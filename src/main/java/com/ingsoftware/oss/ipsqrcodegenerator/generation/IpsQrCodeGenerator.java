/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.generation;

import com.ingsoftware.oss.ipsqrcodegenerator.Constants;
import com.ingsoftware.oss.ipsqrcodegenerator.IpsQrCode;
import com.ingsoftware.oss.ipsqrcodegenerator.IpsQrCodeTag;
import com.ingsoftware.oss.ipsqrcodegenerator.IpsQrCodeType;
import com.ingsoftware.oss.ipsqrcodegenerator.exception.InvalidTagValueException;
import com.ingsoftware.oss.ipsqrcodegenerator.exception.MultipleMutuallyExclusiveTagsException;
import com.ingsoftware.oss.ipsqrcodegenerator.exception.QrCodeGenerationException;
import com.ingsoftware.oss.ipsqrcodegenerator.validation.Validator;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates QR code in image or textual format.
 *
 * @see <a href="https://web.archive.org/web/20200912142737/https://www.nbs.rs/internet/latinica/15/mediji/vesti/20180507_preporuke_QRkod.pdf">NBS recommendations</a>
 */
public class IpsQrCodeGenerator {

    private final QrCodeImageGenerator qrCodeImageGenerator;

    public IpsQrCodeGenerator(QrCodeImageGenerator qrCodeImageGenerator) {
        this.qrCodeImageGenerator = qrCodeImageGenerator;
    }

    /**
     * Generates a <code>BufferedImage</code> object representing the specified {@link IpsQrCode}.
     *
     * @param qrCode qr code instance to be represented visually
     * @param size   size of the image
     * @return <code>BufferedImage</code> object representing the specified {@link IpsQrCode}
     * @throws InvalidTagValueException               if any tag value is invalid
     * @throws MultipleMutuallyExclusiveTagsException if values are set for multiple mutually exclusive tags
     * @throws QrCodeGenerationException              if image generation fails
     */
    public BufferedImage generateImage(IpsQrCode qrCode, int size) throws InvalidTagValueException,
            MultipleMutuallyExclusiveTagsException, QrCodeGenerationException {
        String qrCodeContent = generateTextualContent(qrCode);
        CorrectionLevel correctionLevel = qrCode.getType().getCorrectionLevel();
        return qrCodeImageGenerator.generateBufferedImage(qrCodeContent, size, correctionLevel);
    }

    /**
     * Generates the textual representation of the specified {@link IpsQrCode} if all tag values are valid.
     *
     * @param qrCode qr code instance to be represented textually
     * @return textual representation of the specified {@link IpsQrCode}
     * @throws InvalidTagValueException               if any tag value is invalid
     * @throws MultipleMutuallyExclusiveTagsException if values are set for multiple mutually exclusive tags
     */
    public static String generateTextualContent(IpsQrCode qrCode) throws InvalidTagValueException,
            MultipleMutuallyExclusiveTagsException {
        IpsQrCodeType type = qrCode.getType();
        ExtractedTagValues extractedTagValues = extractTagValues(qrCode, type);

        if (extractedTagValues.mutuallyExclusiveTags.size() > 1) {
            throw new MultipleMutuallyExclusiveTagsException(extractedTagValues.mutuallyExclusiveTags);
        }

        if (extractedTagValues.invalidTags.size() > 0) {
            throw new InvalidTagValueException(extractedTagValues.invalidTags);
        }

        return String.join(Constants.TAG_DELIMITER, extractedTagValues.validTagValues);
    }

    private static ExtractedTagValues extractTagValues(IpsQrCode qrCode, IpsQrCodeType type) {
        ExtractedTagValues extractedTagValues = new ExtractedTagValues();
        boolean mutuallyExclusiveFound = false;

        for (IpsQrCodeTag tag : IpsQrCodeTag.values()) {
            String value = qrCode.getValue(tag);
            if (type.getMandatoryTags().contains(tag)) {
                addTagValue(extractedTagValues, tag, value);
            } else if (type.getMutuallyExclusiveTags().contains(tag) && value != null) {
                extractedTagValues.mutuallyExclusiveTags.add(tag);
                if (!mutuallyExclusiveFound) {
                    addTagValue(extractedTagValues, tag, value);
                    mutuallyExclusiveFound = true;
                }
            } else if (value != null) {
                if (IpsQrCodeTag.getMutuallyExclusiveTags().contains(tag)) {
                    extractedTagValues.mutuallyExclusiveTags.add(tag);
                }
                addTagValue(extractedTagValues, tag, value);
            }
        }

        return extractedTagValues;
    }

    private static void addTagValue(ExtractedTagValues extractedTagValues, IpsQrCodeTag tag, String value) {
        Validator validator = tag.getValidator();
        boolean valid = validator.validate(value);
        if (valid) {
            extractedTagValues.validTagValues.add(tag.name() + Constants.VALUE_DELIMITER + value);
        } else {
            extractedTagValues.invalidTags.add(tag);
        }
    }

    private static class ExtractedTagValues {
        final List<IpsQrCodeTag> invalidTags = new ArrayList<>();
        final List<IpsQrCodeTag> mutuallyExclusiveTags = new ArrayList<>();
        final List<String> validTagValues = new ArrayList<>();
    }

}
