/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.generation;

import com.ingsoftware.oss.ipsqrcodegenerator.exception.QrCodeGenerationException;

import java.awt.image.BufferedImage;

/**
 * Generates a <code>BufferedImage</code> object representing a QR code.
 */
public interface QrCodeImageGenerator {

    /**
     * Generates a <code>BufferedImage</code> object representing a QR code
     * with the specified content, size and correction level.
     *
     * @param qrCodeContent textual QR code content
     * @param size size of square QR code image
     * @param correctionLevel correction level for QR code
     * @return <code>BufferedImage</code> object representing a QR code
     * @throws QrCodeGenerationException if the QR code generation fails
     */
    BufferedImage generateBufferedImage(String qrCodeContent, int size, CorrectionLevel correctionLevel) throws QrCodeGenerationException;

}
