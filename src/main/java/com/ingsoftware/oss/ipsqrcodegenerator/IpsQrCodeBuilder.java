/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import java.util.EnumMap;
import java.util.Map;

/**
 * Builder for new instances of {@link IpsQrCode}.
 *
 * @see <a href="https://ips.nbs.rs/PDF/pdfPreporukeValidacijaEng.pdf">NBS recommendations</a>
 */
public final class IpsQrCodeBuilder {

    private final Map<IpsQrCodeTag, String> content;

    public IpsQrCodeBuilder() {
        content = new EnumMap<>(IpsQrCodeTag.class);
    }

    /**
     * Sets the value of the <em>identification code</em> tag (sr. <i>identifikacioni kod</i>).
     *
     * @param identificationCode value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withIdentificationCode(String identificationCode) {
        content.put(IpsQrCodeTag.K, identificationCode);
        return this;
    }

    /**
     * Sets the value of the <em>version</em> tag (sr. <i>verzija</i>).
     *
     * @param version value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withVersion(String version) {
        content.put(IpsQrCodeTag.V, version);
        return this;
    }

    /**
     * Sets the value of the <em>character set</em> tag (sr. <i>znakovni skup</i>).
     *
     * @param characterSet value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withCharacterSet(String characterSet) {
        content.put(IpsQrCodeTag.C, characterSet);
        return this;
    }

    /**
     * Sets the value of the <em>recipient account number</em> tag (sr. <i>broj racuna primaoca</i>).
     *
     * @param recipientAccountNumber value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withRecipientAccountNumber(String recipientAccountNumber) {
        content.put(IpsQrCodeTag.R, recipientAccountNumber);
        return this;
    }

    /**
     * Sets the value of the <em>recipient name</em> tag (sr. <i>naziv primaoca</i>).
     *
     * @param recipientName value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withRecipientName(String recipientName) {
        content.put(IpsQrCodeTag.N, recipientName);
        return this;
    }

    /**
     * Sets the value of the <em>amount</em> tag (sr. <i>iznos</i>).
     *
     * @param amount value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withAmount(String amount) {
        content.put(IpsQrCodeTag.I, amount);
        return this;
    }

    /**
     * Sets the value of the <em>payer name</em> tag (sr. <i>naziv platioca</i>).
     *
     * @param payerName value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withPayerName(String payerName) {
        content.put(IpsQrCodeTag.P, payerName);
        return this;
    }

    /**
     * Sets the value of the <em>payment code</em> tag (sr. <i>sifra placanja</i>).
     *
     * @param paymentCode value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withPaymentCode(String paymentCode) {
        content.put(IpsQrCodeTag.SF, paymentCode);
        return this;
    }

    /**
     * Sets the value of the <em>payment purpose</em> tag (sr. <i>svrha placanja</i>).
     *
     * @param paymentPurpose value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withPaymentPurpose(String paymentPurpose) {
        content.put(IpsQrCodeTag.S, paymentPurpose);
        return this;
    }

    /**
     * Sets the value of the <em>merchant code category</em> tag (sr. <i>MCC</i>).
     *
     * @param merchantCodeCategory value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withMerchantCodeCategory(String merchantCodeCategory) {
        content.put(IpsQrCodeTag.M, merchantCodeCategory);
        return this;
    }

    /**
     * Sets the value of the <em>one-time code</em> tag (sr. <i>jednokratna sifra</i>).
     *
     * @param oneTimeCode value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withOneTimeCode(String oneTimeCode) {
        content.put(IpsQrCodeTag.JS, oneTimeCode);
        return this;
    }

    /**
     * Sets the value of the <em>reference number</em> tag (sr. <i>poziv na broj</i>).
     *
     * @param referenceNumber value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withReferenceNumber(String referenceNumber) {
        content.put(IpsQrCodeTag.RO, referenceNumber);
        return this;
    }

    /**
     * Sets the value of the <em>recipient reference</em> tag (sr. <i>referenca primaoca</i>).
     *
     * @param recipientReference value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withRecipientReference(String recipientReference) {
        content.put(IpsQrCodeTag.RL, recipientReference);
        return this;
    }

    /**
     * Sets the value of the <em>payment reference</em> tag (sr. <i>referenca placanja</i>).
     *
     * @param paymentReference value
     * @return the same builder instance
     */
    public IpsQrCodeBuilder withPaymentReference(String paymentReference) {
        content.put(IpsQrCodeTag.RP, paymentReference);
        return this;
    }

    /**
     * Creates a new instance of {@link IpsQrCode}.
     *
     * @return the new instance
     */
    public IpsQrCode build() {
        return new IpsQrCode(content);
    }

}
