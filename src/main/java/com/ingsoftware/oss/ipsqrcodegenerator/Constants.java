/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Holds the constant values used in the application.
 */
public final class Constants {

    public static final List<String> ALLOWED_K_VALUES = List.of("PR", "PT", "PK", "EK");
    public static final String ALLOWED_V_VALUE = "01";
    public static final String ALLOWED_C_VALUE = "1";
    public static final String MODEL_97 = "97";
    public static final int MODEL_97_MINUEND = 98;
    public static final long MODEL_97_DIVISOR = 97;
    public static final String ZERO_EXTENSION = "00";
    public static final String TAG_DELIMITER = "|";
    public static final String VALUE_DELIMITER = ":";
    public static final String ACCOUNT_NUMBER_DELIMITER = "-";
    public static final int ASCII_VALUE_DIFFERENCE = 55;

    public static final class Patterns {
        public static final Pattern ONLY_DIGITS_PATTERN = Pattern.compile("^[0-9]+$");
        public static final Pattern ALPHANUMERIC_PATTERN = Pattern.compile("^[0-9a-zA-z]+$");
        public static final Pattern TEXT_PATTERN = Pattern.compile("^[a-zA-ZšđžčćŠĐŽČĆ0-9 (){}\\[\\]<\\/.,:;!@#$%^&?„”“\"`’‘'_~=+\\-\\s]+$");
        public static final Pattern I_PATTERN = Pattern.compile("^RSD[0-9]+,[0-9]{0,2}$");
        public static final Pattern SF_PATTERN = Pattern.compile("^[12][0-9]{2}$");
        public static final Pattern RO_PATTERN = Pattern.compile("^[^97][0-9]{2}([0-9a-zA-z]+-?[0-9a-zA-z])+$|^97[0-9a-zA-z]+$");

        private Patterns() {
        }

    }

    public static final class Limits {

        public static final Limits K = new Limits(2, 3);
        public static final Limits V = new Limits(2);
        public static final Limits C = new Limits(1);
        public static final Limits R = new Limits(18);
        public static final Limits N = new Limits(1, 70, 3);
        public static final Limits I = new Limits(5, 18);
        public static final Limits P = new Limits(0, 70, 3);
        public static final Limits SF = new Limits(3);
        public static final Limits S = new Limits(0, 35, 1);
        public static final Limits M = new Limits(4);
        public static final Limits JS = new Limits(5);
        public static final Limits RO = new Limits(0, 25);
        public static final Limits RL = new Limits(0, 140);
        public static final Limits RP = new Limits(19);

        private final int minLength;
        private final int maxLength;
        private final int lineCount;

        private Limits(int minLength, int maxLength, int lineCount) {
            this.minLength = minLength;
            this.maxLength = maxLength;
            this.lineCount = lineCount;
        }

        private Limits(int minLength, int maxLength) {
            this.minLength = minLength;
            this.maxLength = maxLength;
            this.lineCount = Integer.MAX_VALUE;
        }

        private Limits(int limit) {
            this.minLength = limit;
            this.maxLength = limit;
            this.lineCount = Integer.MAX_VALUE;
        }

        public int getMinLength() {
            return minLength;
        }

        public int getMaxLength() {
            return maxLength;
        }

        public int getLineCount() {
            return lineCount;
        }

    }

    private Constants() {
    }

}
