/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.validation;

import static com.ingsoftware.oss.ipsqrcodegenerator.Constants.*;
import static com.ingsoftware.oss.ipsqrcodegenerator.Constants.Patterns.*;

/**
 * Holds instances of {@link Validator} functions used for validating tag values.
 *
 * @see <a href="https://web.archive.org/web/20210812004310/https://nbs.rs/QRcode/info_g.html">NBS validation info</a>
 */
public final class Validators {

    public static final Validator K = value -> validateLength(value, Limits.K)
            && ALLOWED_K_VALUES.contains(value);

    public static final Validator V = value -> validateLength(value, Limits.V) && value.equals(ALLOWED_V_VALUE);

    public static final Validator C = value -> validateLength(value, Limits.C) && value.equals(ALLOWED_C_VALUE);

    public static final Validator R = value -> {
        if (!validateLength(value, Limits.R) || !ONLY_DIGITS_PATTERN.matcher(value).matches()) return false;
        String accountNumber = value.substring(0, value.length() - 2);
        String control = value.substring(value.length() - 2);
        String actualControl = String.valueOf(calculateMod97(accountNumber));
        return control.equals(actualControl);
    };

    public static final Validator N = value -> validateLength(value, Limits.N)
            && TEXT_PATTERN.matcher(value).matches() && countLines(value) < Limits.N.getLineCount();

    public static final Validator I = value -> validateLength(value, Limits.I)
            && I_PATTERN.matcher(value).matches();

    public static final Validator P = value -> validateLength(value, Limits.P)
            && TEXT_PATTERN.matcher(value).matches() && countLines(value) < Limits.P.getLineCount();

    public static final Validator SF = value -> validateLength(value, Limits.SF) && SF_PATTERN.matcher(value).matches();

    public static final Validator S = value -> validateLength(value, Limits.S)
            && TEXT_PATTERN.matcher(value).matches() && countLines(value) == Limits.S.getLineCount();

    public static final Validator M = value -> validateLength(value, Limits.M)
            && ONLY_DIGITS_PATTERN.matcher(value).matches();

    public static final Validator JS = value -> validateLength(value, Limits.JS)
            && ONLY_DIGITS_PATTERN.matcher(value).matches();

    public static final Validator RO = value -> {
        if (!validateLength(value, Limits.RO) || !RO_PATTERN.matcher(value).matches()) return false;

        String model = value.substring(0, 2);

        if (model.equals(MODEL_97)) {
            int control = Integer.parseInt(value.substring(2, 4));
            String referenceNumber = value.substring(4);
            int i = calculateMod97(referenceNumber);
            return control == i;
        }

        return true;
    };

    public static final Validator RL = value -> validateLength(value, Limits.RL);

    public static final Validator RP = value -> validateLength(value, Limits.RP)
            && ALPHANUMERIC_PATTERN.matcher(value).matches();

    static boolean validateLength(String value, Limits limit) {
        return value != null && value.length() >= limit.getMinLength() && value.length() <= limit.getMaxLength();
    }

    static long countLines(String value) {
        return value.lines().count();
    }

    static long modulo(String dividend, Long divisor) {
        var partLength = 10;
        while (dividend.length() > partLength) {
            var part = dividend.substring(0, partLength);
            dividend = (Long.parseLong(part) % divisor) + dividend.substring(partLength);
        }
        return Long.parseLong(dividend) % divisor;
    }

    static int calculateMod97(String num) {
        return MODEL_97_MINUEND - (int) modulo(num + ZERO_EXTENSION, MODEL_97_DIVISOR);
    }

    private Validators() {
    }

}
