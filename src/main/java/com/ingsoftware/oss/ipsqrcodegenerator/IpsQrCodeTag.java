/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import com.ingsoftware.oss.ipsqrcodegenerator.sanitization.Sanitizer;
import com.ingsoftware.oss.ipsqrcodegenerator.sanitization.Sanitizers;
import com.ingsoftware.oss.ipsqrcodegenerator.validation.Validator;
import com.ingsoftware.oss.ipsqrcodegenerator.validation.Validators;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Tags that an <em>IPS QR code</em> can contain.
 *
 * @see <a href="https://web.archive.org/web/20200912142737/https://www.nbs.rs/internet/latinica/15/mediji/vesti/20180507_preporuke_QRkod.pdf">NBS recommendations</a>
 */
public enum IpsQrCodeTag {

    /**
     * Identification code (sr. <i>identifikacioni kod</i>)
     */
    K(Sanitizers.K, Validators.K, "Identification code"),
    /**
     * Version (sr. <i>verzija</i>)
     */
    V(Sanitizers.DEFAULT, Validators.V, "Version"),
    /**
     * Character set (sr. <i>znakovni skup</i>)
     */
    C(Sanitizers.DEFAULT, Validators.C, "Character set"),
    /**
     * Recipient account number (sr. <i>broj racuna primaoca</i>)
     */
    R(Sanitizers.R, Validators.R, "Recipient account number"),
    /**
     * Recipient name (sr. <i>naziv primaoca</i>)
     */
    N(Sanitizers.DEFAULT, Validators.N, "Recipient name"),
    /**
     * Amount (sr. <i>iznos</i>)
     */
    I(Sanitizers.DEFAULT, Validators.I, "Amount"),
    /**
     * Payer name (sr. <i>naziv platioca</i>)
     */
    P(Sanitizers.DEFAULT, Validators.P, "Payer name"),
    /**
     * Payment code (sr. <i>sifra placanja</i>)
     */
    SF(Sanitizers.DEFAULT, Validators.SF, "Payment code"),
    /**
     * Payment purpose (sr. <i>svrha placanja</i>)
     */
    S(Sanitizers.DEFAULT, Validators.S, "Payment purpose"),
    /**
     * Merchant code category (sr. <i>MCC</i>)
     */
    M(Sanitizers.DEFAULT, Validators.M, "Merchant code category"),
    /**
     * One-time code (sr. <i>jednokratna sifra</i>)
     */
    JS(Sanitizers.DEFAULT, Validators.JS, "One-time code"),
    /**
     * Reference number (sr. <i>poziv na broj</i>)
     */
    RO(Sanitizers.RO, Validators.RO, "Reference number"),
    /**
     * Recipient reference (sr. <i>referenca primaoca</i>)
     */
    RL(Sanitizers.DEFAULT, Validators.RL, "Recipient reference"),
    /**
     * Payment reference (sr. <i>referenca placanja</i>)
     */
    RP(Sanitizers.DEFAULT, Validators.RP, "Payment reference");

    private static final List<IpsQrCodeTag> MUTUALLY_EXCLUSIVE_TAGS = List.of(RO, RL, RP);

    private final Sanitizer sanitizer;
    private final Validator validator;
    private final String fullName;

    IpsQrCodeTag(Sanitizer sanitizer, Validator validator, String fullName) {
        this.sanitizer = sanitizer;
        this.validator = validator;
        this.fullName = fullName;
    }

    public static Optional<IpsQrCodeTag> fromFullName(String name) {
        return Arrays.stream(values())
                .filter(tag -> tag.fullName
                        .toLowerCase()
                        .replace(" ", "-")
                        .equals(name))
                .findFirst();
    }

    public static Optional<IpsQrCodeTag> fromName(String name) {
        return Arrays.stream(values())
                .filter(tag -> tag.name().equals(name))
                .findFirst();
    }

    public Sanitizer getSanitizer() {
        return sanitizer;
    }

    public Validator getValidator() {
        return validator;
    }

    public String getFullName() {
        return fullName;
    }

    public static List<IpsQrCodeTag> getMutuallyExclusiveTags() {
        return MUTUALLY_EXCLUSIVE_TAGS;
    }

}
