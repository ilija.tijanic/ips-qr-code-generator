/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import com.ingsoftware.oss.ipsqrcodegenerator.generation.CorrectionLevel;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Types of <em>IPS QR codes</em> that have different uses.
 * Each type defines a list of mandatory and mutually exclusive tags,
 * as well as the correction level for QR code generation.
 *
 * @see <a href="https://web.archive.org/web/20200912142737/https://www.nbs.rs/internet/latinica/15/mediji/vesti/20180507_preporuke_QRkod.pdf">NBS recommendations</a>
 */
public enum IpsQrCodeType {

    /**
     * Paying bills (sr. <i>placanje racuna</i>)
     */
    PR(List.of(IpsQrCodeTag.K, IpsQrCodeTag.V, IpsQrCodeTag.C, IpsQrCodeTag.R, IpsQrCodeTag.N, IpsQrCodeTag.I, IpsQrCodeTag.P, IpsQrCodeTag.SF, IpsQrCodeTag.S), List.of(IpsQrCodeTag.RO, IpsQrCodeTag.RL), CorrectionLevel.M),
    /**
     * Shown by merchant (sr. <i>prikazuje trgovac</i>)
     */
    PT(List.of(IpsQrCodeTag.K, IpsQrCodeTag.V, IpsQrCodeTag.C, IpsQrCodeTag.R, IpsQrCodeTag.N, IpsQrCodeTag.I, IpsQrCodeTag.SF, IpsQrCodeTag.S, IpsQrCodeTag.M), List.of(IpsQrCodeTag.RO, IpsQrCodeTag.RP), CorrectionLevel.L),
    /**
     * Shown by customer (sr. <i>prikazuje kupac</i>)
     */
    PK(List.of(IpsQrCodeTag.K, IpsQrCodeTag.V, IpsQrCodeTag.C, IpsQrCodeTag.I, IpsQrCodeTag.P, IpsQrCodeTag.S, IpsQrCodeTag.JS), List.of(), CorrectionLevel.L),
    /**
     * E-commerce (sr. <i>internet prodaja</i>)
     */
    EK(List.of(IpsQrCodeTag.K, IpsQrCodeTag.V, IpsQrCodeTag.C, IpsQrCodeTag.R, IpsQrCodeTag.N, IpsQrCodeTag.I, IpsQrCodeTag.SF, IpsQrCodeTag.S, IpsQrCodeTag.M), List.of(IpsQrCodeTag.RO, IpsQrCodeTag.RP), CorrectionLevel.L);

    private final List<IpsQrCodeTag> mandatoryTags;
    private final List<IpsQrCodeTag> mutuallyExclusiveTags;
    private final CorrectionLevel correctionLevel;

    IpsQrCodeType(List<IpsQrCodeTag> mandatoryTags, List<IpsQrCodeTag> mutuallyExclusiveTags, CorrectionLevel correctionLevel) {
        this.mandatoryTags = mandatoryTags;
        this.mutuallyExclusiveTags = mutuallyExclusiveTags;
        this.correctionLevel = correctionLevel;
    }

    public static Optional<IpsQrCodeType> fromIdCode(String idCode) {
        return Arrays.stream(IpsQrCodeType.values())
                .filter(value -> value.name().equals(idCode))
                .findFirst();
    }

    public List<IpsQrCodeTag> getMandatoryTags() {
        return mandatoryTags;
    }

    public List<IpsQrCodeTag> getMutuallyExclusiveTags() {
        return mutuallyExclusiveTags;
    }

    public CorrectionLevel getCorrectionLevel() {
        return correctionLevel;
    }

}
