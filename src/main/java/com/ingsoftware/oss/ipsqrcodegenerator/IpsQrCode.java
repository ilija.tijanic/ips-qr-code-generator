/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import com.ingsoftware.oss.ipsqrcodegenerator.exception.InvalidTagValueException;

import java.util.EnumMap;
import java.util.Map;

/**
 * Represents an <b>IPS QR code</b>, maps tags to values.
 * Default values for <em>V</em> and <em>C</em> tags are mapped during construction.
 *
 * @see <a href="https://ips.nbs.rs/PDF/pdfPreporukeValidacijaEng.pdf">NBS recommendations</a>
 */
public final class IpsQrCode {

    private final Map<IpsQrCodeTag, String> values;

    public IpsQrCode() {
        values = new EnumMap<>(IpsQrCodeTag.class);
        addDefaults();
    }

    public IpsQrCode(Map<IpsQrCodeTag, String> values) {
        this.values = new EnumMap<>(IpsQrCodeTag.class);
        if (values != null) {
            values.forEach(this::putValue);
            addDefaults();
        }
    }

    /**
     * Sanitizes the specified value and associates it with the specified tag in this QR code.
     * If the map previously contained a mapping for the tag, the old value is replaced by the specified value.
     *
     * @param tag   tag with which the specified value is to be associated
     * @param value value to be associated with the specified tag
     */
    public void putValue(IpsQrCodeTag tag, String value) {
        String sanitizedValue = tag.getSanitizer().sanitize(value);
        values.put(tag, sanitizedValue);
    }

    /**
     * Returns the value to which the specified tag is mapped,
     * or {@code null} if this map contains no mapping for the tag.
     *
     * @param tag the tag whose associated value is to be returned
     * @return the value to which the specified key is mapped, or
     * {@code null} if this map contains no mapping for the key
     */
    public String getValue(IpsQrCodeTag tag) {
        return values.get(tag);
    }

    /**
     * Returns the IPS QR code type according to the <em>K</em> tag value.
     *
     * @return the IPS QR code type according to the <em>K</em> tag value.
     * @throws InvalidTagValueException if the <em>K</em> tag value is not valid
     */
    public IpsQrCodeType getType() throws InvalidTagValueException {
        String idCode = values.get(IpsQrCodeTag.K);
        return IpsQrCodeType.fromIdCode(idCode)
                .orElseThrow(() -> new InvalidTagValueException(IpsQrCodeTag.K));
    }

    private void addDefaults() {
        values.putIfAbsent(IpsQrCodeTag.V, "01");
        values.putIfAbsent(IpsQrCodeTag.C, "1");
    }

}
