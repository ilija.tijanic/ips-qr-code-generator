/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.exception;

import com.ingsoftware.oss.ipsqrcodegenerator.IpsQrCodeTag;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a checked exception with the message containing a list of invalid tags.
 */
public class InvalidTagValueException extends Exception {

    public InvalidTagValueException(IpsQrCodeTag tag) {
        super(getMessage(tag));
    }

    public InvalidTagValueException(List<IpsQrCodeTag> tags) {
        super(joinErrorMessages(tags));
    }

    private static String joinErrorMessages(List<IpsQrCodeTag> tags) {
        return tags.stream().map(InvalidTagValueException::getMessage).collect(Collectors.joining("; "));
    }

    private static String getMessage(IpsQrCodeTag tag) {
        return tag.getFullName() + " not valid";
    }

}
