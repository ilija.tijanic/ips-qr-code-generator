/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator;

import com.ingsoftware.oss.ipsqrcodegenerator.generation.IpsQrCodeGenerator;
import com.ingsoftware.oss.ipsqrcodegenerator.generation.ZxingQrCodeImageGenerator;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Main class which enables usage as CLI application.
 */
public class IpsQrCodeGeneratorApp {

    /**
     * Entrypoint for CLI application.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        try {
            String fileFormat = "png";
            String fileName = "qrcode";
            int size = 200;
            IpsQrCode ipsQrCode = new IpsQrCode();

            for (int i = 0; i < args.length; i = i + 2) {
                final String arg = args[i];
                if (arg.equals("--file-name")) {
                    fileName = args[i + 1];
                } else if (arg.equals("--file-format")) {
                    fileFormat = args[i + 1];
                } else if (arg.equals("--size")) {
                    size = Integer.parseInt(args[i + 1]);
                } else if (arg.startsWith("--")) {
                    IpsQrCodeTag tag = IpsQrCodeTag.fromFullName(arg.substring(2).toLowerCase())
                            .orElseThrow(() -> new IllegalArgumentException("Invalid argument name: " + arg));
                    ipsQrCode.putValue(tag, args[i + 1]);
                } else if (arg.startsWith("-")) {
                    IpsQrCodeTag tag = IpsQrCodeTag.fromName(arg.substring(1).toUpperCase())
                            .orElseThrow(() -> new IllegalArgumentException("Invalid argument name: " + arg));
                    ipsQrCode.putValue(tag, args[i + 1]);
                } else {
                    throw new IllegalArgumentException("Invalid arguments");
                }
            }

            IpsQrCodeGenerator ipsQrCodeGenerator = new IpsQrCodeGenerator(new ZxingQrCodeImageGenerator());
            BufferedImage image = ipsQrCodeGenerator.generateImage(ipsQrCode, size);
            ImageIO.write(image, fileFormat, new File(fileName + "." + fileFormat));

            System.out.println("IPS QR code generated");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


}
