/*
 *    Copyright 2023 Ingsoftware d.o.o.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ingsoftware.oss.ipsqrcodegenerator.sanitization;

import com.ingsoftware.oss.ipsqrcodegenerator.Constants;
import com.ingsoftware.oss.ipsqrcodegenerator.Constants.Limits;

import java.util.Optional;

import static com.ingsoftware.oss.ipsqrcodegenerator.Constants.ACCOUNT_NUMBER_DELIMITER;

/**
 * Holds instances of {@link Sanitizer} functions used for sanitizing tag values.
 *
 * @see <a href="https://web.archive.org/web/20210812004310/https://nbs.rs/QRcode/info_g.html">NBS sanitization info</a>
 */
public final class Sanitizers {

    public static final Sanitizer DEFAULT = value -> Optional.ofNullable(value)
            .map(str -> str.replace(Constants.TAG_DELIMITER, "").trim())
            .orElse(null);

    public static final Sanitizer K = value -> Optional.ofNullable(DEFAULT.sanitize(value))
            .map(String::toUpperCase)
            .orElse(null);

    public static final Sanitizer R = value -> Optional.ofNullable(DEFAULT.sanitize(value))
            .map(str -> str.replace(ACCOUNT_NUMBER_DELIMITER, "").replace(" ", "").trim())
            .map(str -> {
                if (str.length() < Limits.R.getMaxLength()) {
                    String bank = str.substring(0, 3);
                    String middle = str.substring(3, str.length() - 2);
                    String end = str.substring(str.length() - 2);
                    return bank + "0".repeat(13 - middle.length()) + middle + end;
                }
                return str;
            })
            .orElse(null);

    public static final Sanitizer RO = value -> Optional.ofNullable(DEFAULT.sanitize(value))
            .map(str -> str.startsWith(Constants.MODEL_97) ? sanitize97ReferenceNumber(str) : str)
            .orElse(null);

    private static String sanitize97ReferenceNumber(String str) {
        final String upperCaseStr = str.toUpperCase();
        return upperCaseStr.substring(0, 4) + convertLettersToNumbers(upperCaseStr.substring(4));
    }

    private static String convertLettersToNumbers(String referenceNumber) {
        StringBuilder stringBuilder = new StringBuilder();

        for (char character : referenceNumber.toCharArray()) {
            if (Character.isLetter(character)) {
                stringBuilder.append(((int) character) - Constants.ASCII_VALUE_DIFFERENCE);
            } else {
                stringBuilder.append(character);
            }
        }

        return stringBuilder.toString();
    }

    private Sanitizers() {
    }

}
